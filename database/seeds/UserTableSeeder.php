<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $name = ['tokwa apor'];
    private $email = ['tokwa'];
    private $password = ['1111111111'];

    public function run()
    {
        for($i = 0; $i < count($this->name); $i++) {
            $x = new User;
            $x->givePermissionTo(['manage users', 'create post', 'edit post', 'publish post', 'unpublish post', 'delete post']);
            $x->name = $this->name[$i];
            $x->email = $this->email[$i] . '@test.com';
            $x->password = Hash::make($this->password[$i]);
            $x->save();
        }
    }
}
