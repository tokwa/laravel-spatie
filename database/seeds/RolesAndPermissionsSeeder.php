<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         // Reset cached roles and permissions
         app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

         // create permissions
         Permission::create(['name' => 'manage users']);
         Permission::create(['name' => 'create post']);
         Permission::create(['name' => 'edit post']);
         Permission::create(['name' => 'publish post']);
         Permission::create(['name' => 'unpublish post']);
         Permission::create(['name' => 'delete post']);
 
         // create roles and assign created permissions
 
         // this can be done as separate statements
         $role = Role::create(['name' => 'visitor']);

         $role = Role::create(['name' => 'writer']);
         $role->givePermissionTo('create post');
         $role->givePermissionTo('edit post');
 
         // or may be done by chaining
         $role = Role::create(['name' => 'moderator'])
             ->givePermissionTo(['publish post', 'unpublish post']);
 
         $role = Role::create(['name' => 'super-admin']);
         $role->givePermissionTo(Permission::all());
    }
}
