<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//admin routes
Route::prefix('admin')->middleware('CanManageUsers')->group( function(){
    Route::get('/', 'AdminController@index')->name('admin.index');
    Route::post('/', 'AdminController@store')->name('admin.store');
    Route::put('/user/{id}', 'AdminController@update')->name('admin.update');
    Route::delete('/user{id}', 'AdminController@destroy')->name('admin.destroy');
    Route::delete('/', 'AdminController@deleteMultiple')->name('admin.deleteMultiple');

    Route::get('/posts', 'AdminController@posts')->name('admin.posts');
    Route::get('/posts/{id}', 'AdminController@editpost')->name('admin.editpost');
    Route::put('/posts/{id}', 'AdminController@updatepost')->name('admin.updatepost');
    Route::delete('/posts/{id}', 'AdminController@destroypost')->name('admin.postdestroy');
});

// post routes
Route::resource('posts', 'PostController');

// comment route
Route::post('/posts/{id}', 'CommentController@store')->name('comment.store');

