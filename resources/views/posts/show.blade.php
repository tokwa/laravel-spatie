@extends('layouts.app')

@section('title')
Show
@endsection

@section('content')
<main>
    <div class="container d-flex justify-content-center">
        <div class="card" style="width: 35rem;">
            <img class="card-img-top" src="{{asset('/images/posts/' . $post->image)}}" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">{{$post->title}}</h5>
                <small>By: {{$post->user->name}} </small>
                <br>
                <small>Created on: {{$post->created_at->format('M d Y @ H:m:ss')}} </small>
                <br>
                <small>Related Tags:
                    @foreach($post->tags as $tag)
                        {{$tag->name}}
                        {{ !$loop->last ? ',' : '' }}
                    @endforeach
                </small>
                <br><br>
                <a href="{{route('posts.index')}}" class="btn btn-outline-primary">Go Back</a>
                <hr>
                <p>
                    {!! html_entity_decode($post->body) !!}
                </p>
                <p class="card-text">
                    {{$post->comment}}
                </p>
                <hr>
                <p>
                @if(count($post->comments))
                Comments:
                <br>
                @foreach($post->comments as $x)
                    <small> <span class="text-primary">{{$x->user->name}}</span>: {{$x->comment}} </small>
                    <br>
                @endforeach
                @else
                    <p>
                        There is no comment for this post.
                    </p>
                @endif
                </p>
            </div>
        </div>
        <br>
    </div>

    <div class="container mt-5 d-flex justify-content-center">
        <form action="" method="POST" id="commentForm" style="width: 35rem;">
            <div class="form-group">
                <label for="comment">Comment this post.</label>
                <textarea class="form-control" id="comment" name="comment" rows="3"></textarea>
                <div class="invalid-feedback" id="err-comment" role="alert"></div>
                <small id="titleHelp" class="form-text text-muted">Post content.</small>
            </div>
            <div class="form-group">
                <a class="btn btn-outline-danger" href="{{route('posts.index')}}">Cancel</a>
                <button type="submit" id="submitBtn" class="btn btn-outline-primary">
                    {{ __('Submit') }}
                </button>
            </div>
        </form>
    </div>
</main>

<script>
    $(document).ready(() => {
        document.getElementById('commentForm').addEventListener('submit', (e) => {
            e.preventDefault()

            const x = new FormData()
            x.append('comment', document.getElementById('comment').value)

            axios.post("{{ route('comment.store', $post->id) }}", x)
                .then((response) => {
                    console.log(response.data)
                    alert(response.data.sms)
                    window.location.reload()
                })

                .catch((error) => {
                    console.log(error.response)

                    const errSms = document.getElementsByClassName('invalid-feedback')
                    Array.from(errSms).forEach(el => el.innerHTML = "")
                    Array.from(errSms).forEach(el => el.style.display = "none")

                    if (error.response.data.errors.comment) {
                            let box = document.querySelector('comment')
                            let sms = document.getElementById('err-comment')
                            sms.innerHTML = error.response.data.errors.comment[0]
                            sms.style.display = "block"
                    }
                })
        })
    });
</script>
@endsection