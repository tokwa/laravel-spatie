@extends('layouts.app')

@section('title')
Show
@endsection

@section('content')
<div class="container">
        {{ csrf_field() }}
        <form action="" method="POST" id="editForm" enctype="multipart/form-data">
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" class="form-control" id="title" name="title" aria-describedby="title-Help"
                    value="{{$post->title}}">
                <div class="invalid-feedback" id="err-title" role="alert"></div>
                <small id="titleHelp" class="form-text text-muted">Title of the post.</small>
            </div>
            <div class="form-group">
                <label for="body">Post</label>
                <textarea class="form-control my-editor" id="body" name="body" rows="3">{{$post->body}}</textarea>
                <div class="invalid-feedback" id="err-body" role="alert"></div>
                <small id="titleHelp" class="form-text text-muted">Post content.</small>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-6">
                        <strong class="text-primary">Old</strong>
                        <br>
                        <img src="{{asset('images/posts/' . $post->image)}}" alt="" style="height: 200px; width: 100%; object-fit: cover;">
                    </div>
                    <div class="col-6" id="">
                        <strong class="text-primary">New</strong>
                        <br>
                        <img id="imagePreview" class="editImagePreview" style="height: 200px; width: 100%; object-fit: cover;">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <strong>Change image: &nbsp;</strong>
                <input type="file" id="image" name="image" accept="image/*" onchange="loadFile(event)">
                <div class="invalid-feedback" id="err-image" role="alert"></div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <strong class="text-muted mb-3 mt-4 d-block">Tags:</strong>
                    @if(count($tags))
                        @foreach($tags as $tag)
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" name="tags" class="custom-control-input text-lowercase row_check" 
                            id="{{ "tag-".$tag->name}}" 
                            value="{{$tag->id}}"
                            @foreach($post->tags as $x)
                                @if($x->id == $tag->id)
                                    checked
                                @endif
                            @endforeach
                            >
                            <label class="custom-control-label text-lowercase" for="{{ "tag-". utf8_decode($tag->name) }}">
                                {{$tag->name}}
                            </label>
                        </div>
                        @endforeach
                    @else
                        <p>There is no tag at the moment.</p>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <a class="btn btn-danger" href="{{route('posts.index')}}">Cancel</a>
                <button type="submit" id="submitBtn" class="btn btn-primary">
                    {{ __('Submit') }}
                </button>
            </div>
        </form>
</div>

<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script src="{{ asset('js/tinyMCE.js') }}"></script>
<script>
    // preview image script
    
    var loadFile = function(event) {
            var reader = new FileReader();
            reader.onload = function(){
                var output = document.getElementById('imagePreview');
                output.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
        };

    $(document).ready(() => {

        document.getElementById('editForm').addEventListener('submit', function (e) {
            e.preventDefault()

        var postIdsArr = [];

        $('.row_check:checked').each( function() {
            postIdsArr.push($(this).val())
        })

        var postStrIds = postIdsArr.join(',')

        console.log(postStrIds)

        const x = new FormData()
        x.append('title', document.getElementById('title').value)
        x.append('body', document.getElementById('body').value)
        x.append('image', document.getElementById('image').files[0])
        x.append('tags', postStrIds)
        x.append('_method', 'PUT')
        
        axios.post("{{ route('posts.update', $post->id) }}", x)
            .then((response) => {
                alert(response.data.sms)
                window.location.href = "{{ route('posts.index') }}"
        })

        .catch((error) => {
            console.log(error.response)

        })
    })
    });
</script>
@endsection