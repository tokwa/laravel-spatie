@extends('layouts.app')

@section('title')
Home
@endsection

@section('content')
<main>
    <div class="container">
        <ul class="list-group">
            @if(count($posts))
            @foreach($posts as $post)
            <li class="list-group-item list-group-item-warning text-center" id="post-{{$post->id}}">
                <div class="row">
                    <div class="col-6">
                        {{$post->title}}
                        <br>
                            <img src="{{asset('images/posts/' . $post->image)}}" alt="" style="height: 100px; width: 150px; object-fit: cover;">
                        <br>
                        <small class="text-primary">By: {{$post->user->name}}</small>
                        <br>
                        <small>Related Tags: 
                            @foreach($post->tags as $tag)
                                {{$tag->name}}
                                {{!$loop->last ? ',' : '' }}
                            @endforeach
                        </small>
                    </div>
                    <div class="col-6 text-right">
                        <a class="btn btn-outline-primary" href="{{route('posts.show', $post->slug)}}">View</a>
                        @can('edit post')
                        <a class="btn btn-outline-success" href="{{route('posts.edit', $post->slug)}}">Edit</a>
                        @endcan
                        @can('delete post')
                        {{-- <button class="btn btn-outline-danger ti-trash" data-toggle="modal"
                                data-target="#deleteUser"
                                data-name='Delete post "<strong>{{ $post->title }}</strong>"?'
                                data-url="{{ route('posts.destroy', $post->id) }}">Delete
                        </button> --}}
                        <button class="btn btn-outline-danger ti-trash btn-delete"
                                data-id="post-{{$post->id}}"
                                data-title='Delete post "<strong>{{ $post->title }}</strong>"?'
                                data-url="{{ route('posts.destroy', $post->id) }}">Delete
                        </button>
                        @endcan
                    </div>
                </div>
            </li>
            @endforeach
            @else
            <p>
                There is no post at the moment.
            </p>
            @endif
        </ul>
    </div>
    <div class="d-flex justify-content-center mt-3">
            {{$posts->links()}}
    </div>
</main>

@can('delete post')
{{-- Start of Delete Modal --}}
<div class="modal fade" id="deletePost" tabindex="-1" role="dialog" aria-labelledby="deleteModalTitle"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="color: black;">
            <div class="modal-header">
                <h6 class="modal-title" id="deleteModalTitle">Delete?</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid d-none alert  mb-3 text-center" id="deleteErrBox">
                    <h5><span id="deleteMsg"></span></h5>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
                <form action="" method="POST" id="delete_user_form">
                    <button type="submit" class="btn btn-outline-danger" id="btn-modal-delete">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>
{{-- End of Delete Modal --}}
<script>
    $(document.body).on('click','.btn-delete',function(){
        var id = $(this).data('id'),
        modalll = $('#deletePost'),
        title = $(this).data('title');
        url = $(this).data('url');

        modalll.modal('show');
        $('#deleteErrBox').addClass('d-none')
        
        modalll.on('shown.bs.modal',function(){
            $('#btn-modal-delete').data('id',id);
            $('#btn-modal-delete').data('url',url);
            $('#deleteModalTitle').html(title);
        });

    });

    $(document.body).on('click','#btn-modal-delete',function(e){
        e.preventDefault();

        var id = $(this).data('id'),
        url = $(this).data('url');
        axios.delete(url)
            .then(response => {    
                if (response.data.error) {
                    console.log(response.data.error)
                    $('#deleteErrBox').removeClass('d-none')
                    $('#deleteErrBox').addClass('alert-danger')
                    $('#deleteMsg').html(response.data.error)
                } else {
                    console.log('success')
                    $('#deleteErrBox').removeClass('d-none')
                    $('#deleteErrBox').addClass('alert-success')
                    $('#deleteMsg').html('Post Deleted')
                    $('#'+id).fadeOut()
                    $('#deletePost').modal('hide'); 
                }
            })
        .catch(error => {
            console.log(error.response)
            alert(error.response.error)
        })
    })
    // $(document).ready(() => {
    //     $('#deleteUser').on('show.bs.modal', function (e) {
    //         let btn = $(e.relatedTarget)
    //         let modal = $(this)
    //         let name = modal.find('.modal-content #deleteModalTitle').html(btn.data('name'))
    //         let url = modal.find('.modal-content #delete_user_form').attr('action', btn.data('url'))

    //         document.getElementById('delete_user_form').addEventListener('submit', function (e) {
    //             e.preventDefault()
    //             axios.delete(this.action)
    //                 .then(response => {

    //                     if (response.data.error) {
    //                         // alert(response.data.error)
    //                         $('#deleteErrBox').removeClass('d-none')
    //                         $('#deleteErrBox').addClass('alert-danger')
    //                         $('#deleteMsg').html(response.data.error)
    //                     } else {
    //                         console.log(response.data.sms)
    //                         $('#deleteErrBox').removeClass('d-none')
    //                         $('#deleteErrBox').addClass('alert-success')
    //                         $('#deleteMsg').html('Post Successfully Deleted')
    //                         window.location.reload()
    //                     }
    //                 })
    //                 .catch((error) => {
    //                     console.log(error.response)
    //                     let genErr = document.getElementById('deleteUserErr')
    //                     genErr.innerHTML = err.response.statusText
    //                     genErr.classList.remove('d-none')
    //                 })
    //         });
    //     });
    // });
</script>
@endcan
@endsection


{{-- 
if (response.data.error) {
    // alert(response.data.error)
    $('#deleteErrBox').removeClass('d-none')
    $('#deleteErrBox').addClass('alert-danger')
    $('#deleteMsg').html(response.data.error)
} else {
    console.log(response.data.sms)
    $('#deleteErrBox').removeClass('d-none')
    $('#deleteErrBox').addClass('alert-success')
    $('#deleteMsg').html('Post Successfully Deleted')
    window.location.reload()
}
})
.catch((error) => {
console.log(error.response)
let genErr = document.getElementById('deleteUserErr')
genErr.innerHTML = err.response.statusText
genErr.classList.remove('d-none')
}) --}}