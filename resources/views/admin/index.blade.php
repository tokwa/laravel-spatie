
@extends('layouts.admin-layout')

@section('page_location')
Users
@endsection

@section('admin-content')
<div class="container-fluid">
    <div class="col-lg mt-5">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">
                    <button type="button" class="btn btn-md btn-outline-success ti-user" data-toggle="modal"
                        data-target="#createuser">
                        Create User
                    </button>
                    <button class="btn btn-outline-danger" type="button" id="deleteSelectedUsers">
                        <i class="fas fa-fw fa-trash"></i> Delete Selected
                    </button>
                </h4>
                <div class="single-table">
                    <div class="table-responsive">
                        <table class="table text-center">
                            <thead class="text-uppercase bg-primary">
                                <tr class="bg-dark text-white">
                                    <th scope="col">
                                        <div class="custom-control custom-checkbox custom-control-inline">
                                            <input type="checkbox" id="select_all_users"
                                                class="custom-control-input text-lowercase">
                                            <label class="custom-control-label text-uppercase"
                                                for="select_all_users"></label>
                                        </div>
                                    </th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Role</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Created At</th>
                                    <th scope="col">Member Since</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($users))
                                @foreach($users as $user)
                                <tr class="bg-dark text-white">
                                    <td scope="row">
                                        <div class="custom-control custom-checkbox custom-control-inline">
                                            <input type="checkbox" name="user"
                                                class="custom-control-input text-lowercase row_check_all_users"
                                                id="{{ $user->name }}" value="{{ $user->id }}" data-id="{{$user->id}}">
                                            <label class="custom-control-label text-lowercase"
                                                for="{{ $user->name }}"></label>
                                        </div>
                                    </td>
                                    <td> {{$user->name}} </td>
                                    <td>
                                        {{-- {{$user->getPermissionNames()->implode(' | ')}} --}}
                                        @php
                                        $dpArr = [];
                                        @endphp

                                        @if( !count($user->getDirectPermissions()) )
                                        <strong>No direct permission(s) found.</strong>
                                        @else

                                        @foreach ($user->getDirectPermissions() as $gdp)

                                        @php
                                        $dpArr[] = $gdp->id . ',';
                                        @endphp

                                        <strong class="badge badge-info">{{ $gdp->name }}</strong>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(!$user->email_verified_at)
                                        <span class="text-danger">Inactive</span>
                                        @else
                                        <span class="text-primary">Active</span>
                                        @endif
                                    </td>
                                    <td> {{$user->email}} </td>
                                    <td> {{$user->created_at}} </td>
                                    <td>
                                        @if(!$user->email_verified_at)
                                        <span class="text-danger">-&nbsp;&nbsp;-&nbsp;&nbsp;-</span>
                                        @else
                                        {{$user->email_verified_at}}
                                        @endif
                                    </td>
                                    <td>
                                        <button class="btn btn-outline-success ti-pencil" data-toggle="modal"
                                            data-target="#edituser" data-editname="{{ $user->name }}"
                                            data-editemail="{{ $user->email }}" data-directp="{{ implode($dpArr) }}"
                                            data-url="{{ route('admin.update', $user->id) }}">
                                        </button>

                                        <button class="btn btn-outline-danger ti-trash" data-toggle="modal"
                                            data-target="#deleteUser"
                                            data-name='Delete user "<strong>{{ $user->name }}</strong>"?'
                                            data-url="{{ route('admin.destroy', $user->id) }}">
                                        </button>
                                    </td>
                                    @endforeach
                                    @else
                                    <p>There is no user at the moment.</p>
                                    @endif
                            </tbody>
                        </table>
                        <br>
                        <p>Total number of users: {{count($users)}}</p>
                        <div class="container-fluid d-flex justify-content-center mt-3">
                            <span class="bg-dark text-white">{{$users->links()}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Start of Delete Modal --}}

<div class="modal fade" id="deleteUser" tabindex="-1" role="dialog" aria-labelledby="deleteModalTitle"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="color: black;">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalTitle">Delete?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                    <div class="container-fluid d-none alert  mb-3 text-center" id="deleteErrBox">
                            <h5><strong><span id="deleteMsg"></span></strong></h5>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
                <form action="" method="POST" id="delete_user_form">
                    <button type="submit" class="btn btn-outline-danger">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>

{{-- End of Delete Modal --}}

<!-- Create User Modal -->
<div class="modal resetCreateModal fade" id="createuser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create New User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="" id="createForm">
                @csrf
                <div class="modal-body">
                    <div class="container-fluid d-none alert  mb-3 text-center" id="genErrBox">
                        <strong><span id="msg"></span></strong>
                    </div>

                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{
                                                    __('Name') }}</label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}"
                                autofocus>
                            <div class="invalid-feedback" id="err-name" role="alert"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right">{{
                                                    __('E-Mail Address') }}</label>

                        <div class="col-md-6">
                            <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}">
                            <div class="invalid-feedback" id="err-email" role="alert"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="permissions" class="col-md-4 col-form-label text-md-right">Permission</label>

                        <div class="col-md-6">
                            @if(count($permissions))
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="checkbox" id="select_all"
                                    class="custom-control-input text-lowercase row_check_all">
                                <label class="custom-control-label text-uppercase" for="select_all"><strong>select
                                        all</strong></label>
                            </div>
                            <br><br>
                            @foreach($permissions as $permission)
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="checkbox" name="permissions"
                                    class="custom-control-input text-lowercase row_check"
                                    id="{{ utf8_decode($permission->name) }}" value="{{ $permission->id }}"
                                    data-id="{{$permission->id}}">
                                <label class="custom-control-label text-lowercase"
                                    for="{{ utf8_decode($permission->name) }}">
                                    {{ utf8_decode($permission->name) }}</label>
                            </div>
                            @endforeach
                            @else
                            <p>
                                There is no permission available.
                            </p>
                            @endif
                            <div class="invalid-feedback" id="err-permissions" role="alert"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password" class="col-md-4 col-form-label text-md-right">{{
                                                    __('Password') }}</label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control" name="password">
                            <div class="invalid-feedback" id="err-password" role="alert"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{
                                                    __('Confirm Password') }}</label>

                        <div class="col-md-6">
                            <input id="password-confirm" type="password" class="form-control"
                                name="password_confirmation">
                            <div class="invalid-feedback" id="err-password-confirm" role="alert"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    <button id="submitBtn" type="submit" class="btn btn-primary">
                        {{ __('Register') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- End of Create User Modal --}}

{{-- Start of Edit User modal --}}

<div class="modal fade" id="edituser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid d-none alert text-center" id="editErrorBox" role="alert">
                    <strong><span id="editMsg"></span></strong>
                </div>
                <form method="POST" action="" id="update_user_form">
                    @csrf

                    <div class="form-group row">
                        <label for="editname" class="col-md-4 col-form-label text-md-right">{{
                                __('Name') }}</label>

                        <div class="col-md-6">
                            <input id="editname" type="text" class="form-control" name="name" autofocus>
                            <div class="invalid-feedback" id="err-editname" role="alert"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="editemail" class="col-md-4 col-form-label text-md-right">{{
                                __('E-Mail Address') }}</label>

                        <div class="col-md-6">
                            <input id="editemail" type="text" class="form-control" name="email">
                            <div class="invalid-feedback" id="err-editemail" role="alert"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="editpassword" class="col-md-4 col-form-label text-md-right">{{
                                    __('Password') }}</label>

                        <div class="col-md-6">
                            <input id="editpassword" type="password" class="form-control" name="password">
                            <div class="invalid-feedback" id="err-editpassword" role="alert"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="edit-password-confirm" class="col-md-4 col-form-label text-md-right">{{
                                                        __('Confirm Password') }}</label>

                        <div class="col-md-6">
                            <input id="edit-password-confirm" type="password" class="form-control"
                                name="password_confirmation">
                            <div class="invalid-feedback" id="err-edit-password-confirm" role="alert"></div>
                        </div>
                    </div>


                    <div class="form-group row">
                        <label for="role" class="col-md-4 col-form-label text-md-right">Permission</label>

                        <div class="col-md-6">
                            @if(count($permissions))
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="checkbox" id="select_all_permissions"
                                    class="custom-control-input text-lowercase row_check_all_permissions">
                                <label class="custom-control-label text-uppercase"
                                    for="select_all_permissions"><strong>select
                                        all</strong></label>
                            </div>
                            <br><br>
                            @if( !count($permissions) )
                            <p>No permission(s) found.</p>
                            @else

                            @foreach( $permissions as $p )
                            <label class="custom-control custom-checkbox custom-control-inline">
                                <input type="checkbox" id="p_{{ $p->id }}_u" name="u_permissions"
                                    class="custom-control-input row_check_permissions" value="{{ $p->id }}">
                                <span class="custom-control-label">{{ $p->name }}</span>
                            </label>
                            @endforeach
                            @endif
                            @endif
                            <div class="invalid-feedback" id="err-permissions" role="alert"></div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                <button id="editBtn" type="submit" class="btn btn-primary">
                    {{ __('Update') }}
                </button>
            </div>
            </form>
        </div>
    </div>
</div>
</div><!-- .content -->

{{-- End of Edit User modal --}}


<script src="{{ asset('js/app.js') }}"></script>

<script>
    $(document).ready(() => {

        $('.modal').on('hidden.bs.modal', function () {
            console.log('Modal closed.')
            $('#genErrBox').addClass('d-none')
            $('#editErrorBox').addClass('d-none')
            $(this).find('form').trigger('reset')
        });

        $('.resetCreateModal').on('show.bs.modal', function () {
            $(this).find('form').trigger('reset')
        });

        // create select all method

        $('#select_all').on('click', (e) => {
            $('.row_check').prop('checked', $(e.target).prop('checked'));
        })

        $('.row_check').on('click', () => {
            if ($('.row_check:checked').length == $('.row_check').length) {
                $('#select_all').prop('checked', true)
            } else {
                $('#select_all').prop('checked', false)
            }
        })

        document.getElementById('createForm').addEventListener('submit', (e) => {
            e.preventDefault()

            $('#submitBtn').html('<i class="fas fa-spinner fa-spin"></i> Uploading ..')
            $('#submitBtn').prop('disabled', true)

            var idsArr = [];

            $('.row_check:checked').each(function () {
                idsArr.push($(this).val())
            })

            var strIds = idsArr.join(',')

            console.log(strIds)

            const x = new FormData()
            x.append('name', document.getElementById('name').value)
            x.append('email', document.getElementById('email').value)
            x.append('permissions', strIds)
            x.append('password', document.getElementById('password').value)
            x.append('password_confirmation', document.getElementById('password-confirm').value)

            axios.post("{{ route('admin.store') }}", x)
                .then((response) => {
                    console.log(response.data)
                    $('#genErrBox').removeClass('d-none')
                    $('#genErrBox').removeClass('alert-danger')
                    $('#genErrBox').addClass('alert-success')
                    $('#msg').html(response.data.sms)
                    window.location.href = "{{ route('admin.index') }}"
                })

                .catch((error) => {

                    $('#submitBtn').html('Submit')
                    $('#submitBtn').prop('disabled', false)

                    $('#genErrBox').removeClass('d-none')
                    $('#genErrBox').addClass('alert-danger')

                    console.log(error.response) // this is to print the specific server errors

                    const errBoxes = document.getElementsByClassName('form-control')
                    const errSms = document.getElementsByClassName('invalid-feedback')
                    Array.from(errBoxes).forEach(el => el.classList.remove('is-invalid'))
                    Array.from(errSms).forEach(el => el.innerHTML = "")
                    Array.from(errSms).forEach(el => el.style.display = "none")

                    if (error.response.data.errors.name) {
                        let box = document.querySelector('#name')
                        let sms = document.getElementById('err-name')
                        sms.innerHTML = error.response.data.errors.name[0]
                        sms.style.display = "block"
                        box.classList.add('is-invalid')
                    }

                    if (error.response.data.errors.email) {
                        let box = document.querySelector('#email')
                        let sms = document.getElementById('err-email')
                        sms.innerHTML = error.response.data.errors.email[0]
                        sms.style.display = "block"
                        box.classList.add('is-invalid')
                    }

                    if (error.response.data.errors.permissions) {
                        let box = document.querySelector('#{{$permission->name}}')
                        let sms = document.getElementById('err-permissions')
                        sms.innerHTML = error.response.data.errors.permissions[0]
                        sms.style.display = "block"
                        box.classList.add('is-invalid')
                    }

                    if (error.response.data.errors.password) {
                        let box = document.querySelector('#password')
                        let sms = document.getElementById('err-password')
                        sms.innerHTML = error.response.data.errors.password[0]
                        sms.style.display = "block"
                        box.classList.add('is-invalid')
                    }

                    if (error.response.data.errors.password_confirmation) {
                        let box = document.querySelector('#password-confirm')
                        let sms = document.getElementById('err-password-confirm')
                        sms.innerHTML = error.response.data.errors.password_confirmation[0]
                        sms.style.display = "block"
                        box.classList.add('is-invalid')
                    }

                    $('#msg').html(error.response.data.errors = "Enter a value to all fields!")

                });
        });


        // edit select all method

        $('#select_all_permissions').on('click', (e) => {
            $('.row_check_permissions').prop('checked', $(e.target).prop('checked'));
        })

        $('.row_check_permissions').on('click', () => {
            if ($('.row_check_permissions:checked').length == $('.row_check_permissions').length) {
                $('#select_all_permissions').prop('checked', true)
            } else {
                $('#select_all_permissions').prop('checked', false)
            }
        })

        $('#edituser').on('show.bs.modal', function (e) {
            var btn = $(e.relatedTarget)
            var modal = $(this)

            var editname = modal.find('.modal-body #editname').val(btn.data('editname'))
            var editemail = modal.find('.modal-body #editemail').val(btn.data('editemail'))
            var edipermissions = modal.find('.modal-content #update_user_form').attr('action', btn.data(
                'url'))
            var directPermissions = btn.data('directp')
            var directPermissionsArr = directPermissions.split(',')

            $('input:checkbox[name=u_permissions]').each(function () {
                if (directPermissionsArr.includes($(this).val())) {
                    $(this).prop('checked', true)
                }
            });

            document.getElementById('update_user_form').addEventListener('submit', function (e) {
                e.preventDefault()

                $('#editBtn').html('<i class="fas fa-spinner fa-spin"></i> Uploading ..')
                $('#editBtn').prop('disabled', true)

                let idsArr = []
                let pIdsArr = []

                $('input:checkbox[name=u_permissions]:checked').each(function () {
                    pIdsArr.push($(this).val())
                });

                let x = {
                    name: document.getElementById('editname').value,
                    email: document.getElementById('editemail').value,
                    password: document.getElementById('editpassword').value,
                    password_confirmation: document.getElementById('edit-password-confirm').value,
                    permissions: pIdsArr
                }

                console.log(x)

                axios.put(this.action, x)
                    .then((response) => {
                        console.log(response.data.sms)
                        $('#editErrorBox').removeClass('d-none')
                        $('#editErrorBox').removeClass('alert-danger')
                        $('#editErrorBox').addClass('alert-success')
                        $('#editMsg').html(response.data.sms)
                        window.location.reload()
                    })

                    .catch((error) => {

                        console.log(error.response) // this is to print the specific server errors

                        $('#editBtn').html('Update')
                        $('#editBtn').prop('disabled', false)

                        const errBoxes = document.getElementsByClassName('form-control')
                        const errSms = document.getElementsByClassName('invalid-feedback')
                        Array.from(errBoxes).forEach(el => el.classList.remove('is-invalid'))
                        Array.from(errSms).forEach(el => el.innerHTML = "")
                        Array.from(errSms).forEach(el => el.style.display = "none")
                        $('#editErrorBox').removeClass('d-none')
                        $('#editErrorBox').addClass('alert-danger')

                        if (error.response.data.errors.name) {
                            let box = document.querySelector('#editname')
                            let sms = document.getElementById('err-editname')
                            sms.innerHTML = error.response.data.errors.name[0]
                            sms.style.display = "block"
                            box.classList.add('is-invalid')
                        }

                        if (error.response.data.errors.email) {
                            let box = document.querySelector('#editemail')
                            let sms = document.getElementById('err-editemail')
                            sms.innerHTML = error.response.data.errors.email[0]
                            sms.style.display = "block"
                            box.classList.add('is-invalid')
                        }

                        if (error.response.data.errors.password) {
                            let box = document.querySelector('#editpassword')
                            let sms = document.getElementById('err-editpassword')
                            sms.innerHTML = error.response.data.errors.password[0]
                            sms.style.display = "block"
                            box.classList.add('is-invalid')
                        }

                        $('#editMsg').html(error.response.data.errors =
                            "There is an / are error in your input / s!")
                    });
            });
        });

        $('#deleteUser').on('show.bs.modal', function (e) {
            let btn = $(e.relatedTarget)
            let modal = $(this)
            let name = modal.find('.modal-content #deleteModalTitle').html(btn.data('name'))
            let url = modal.find('.modal-content #delete_user_form').attr('action', btn.data('url'))

            document.getElementById('delete_user_form').addEventListener('submit', function (e) {
                e.preventDefault()
                axios.delete(this.action)
                    .then(response => {
                                
                        if (response.data.error) {
                            alert(response.data.error)
                            $('#deleteErrBox').removeClass('d-none')
                            $('#deleteErrBox').addClass('alert-success')
                            $('#deleteMsg').html(response.data.error)
                        } else {
                            console.log(response.data.sms)
                            $('#deleteErrBox').removeClass('d-none')
                            $('#deleteErrBox').addClass('alert-success')
                            $('#deleteMsg').html('User Deleted')
                            window.location.reload()
                        }
                    })
                    .catch(error => {
                        console.log(error.response.data.error)
                        alert(error.response.data.error)
                    })
            });
        });

        // Select All Users

        $('#select_all_users').on('click', (e) => {
            $('.row_check_all_users').prop('checked', $(e.target).prop('checked'));
        })

        $('.row_check_all_users').on('click', () => {
            if ($('.row_check_all_users:checked').length == $('.row_check_all_users').length) {
                $('#select_all_users').prop('checked', true)

            } else {
                $('#select_all_users').prop('checked', false)
            }
        })


        $('#deleteSelectedUsers').on('click', () => {

            let ids = [];

            $('.row_check_all_users:checked').each(function () {
                ids.push($(this).val())
            })

            if (ids.length <= 0) {
                alert('Please select at least one record to delete')
            } else {
                if (confirm('Are you sure, you want to delete the selected items?')) {
                    let x = {
                        _method: 'DELETE',
                        ids
                    }
                    console.log(ids)
                    axios.post("{{ route('admin.deleteMultiple') }}", x)
                    .then( (response) => {
                        // alert(response.data.sms)
                        if (response.data.error) {
                            alert(response.data.error)                            
                        } else {
                            alert('User/s successfully deleted')
                            window.location.reload()
                        }
                    })

                    .catch((error) => {
                        alert(error.response.sms)
                    })
                }
            }

        });

    });
</script>

@endsection
