@extends('layouts.admin-layout')

@section('page_location')
Posts
@endsection

@section('admin-content')
<div class="container-fluid">
    <div class="col-lg mt-5">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">
                    <a href="{{route('posts.create')}} " class="btn btn-md btn-outline-success ti-user">
                        Create New Post
                    </a>
                    <button class="btn btn-outline-danger" type="button" id="deleteSelectedUsers">
                        <i class="fas fa-fw fa-trash"></i> Delete Selected
                    </button>
                </h4>
                <div class="single-table">
                    <div class="table-responsive">
                        <table class="table text-center">
                            <thead class="text-uppercase bg-primary">
                                <tr class="bg-dark text-white">
                                    <th scope="col">
                                        <div class="custom-control custom-checkbox custom-control-inline">
                                            <input type="checkbox" id="select_all_posts"
                                                class="custom-control-input text-lowercase row_check_all_posts">
                                            <label class="custom-control-label text-uppercase"
                                                for="select_all_posts"></label>
                                        </div>
                                    </th>
                                    <th scope="col">Title</th>
                                    <th scope="col">Image</th>
                                    <th scope="col">Created By</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Created At</th>
                                    <th scope="col">A / R</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($posts))
                                @foreach($posts as $post)
                                <tr class="bg-dark text-white">
                                    <td scope="row">
                                        <div class="custom-control custom-checkbox custom-control-inline">
                                            <input type="checkbox" name="post"
                                                class="custom-control-input text-lowercase row_check_post"
                                                id="{{ $post->id }}" value="{{ $post->id }}" data-id="{{$post->id}}">
                                            <label class="custom-control-label text-lowercase"
                                                for="{{ $post->id }}"></label>
                                        </div>
                                    </td>
                                    <td> {{$post->title}} </td>
                                    <td> <img src="{{asset('/images/posts/' . $post->image)}} " alt="Post image"
                                            style="width: 75px; height: 50px; object-fit: cover;"> </td>
                                    <td> {{$post->user->name}} </td>
                                    <td>
                                        @if($post->status == 1)
                                        <span class="text-primary">active</span>
                                        @elseif($post->status == 0)
                                        <span class="text-danger">pending</span>
                                        @else
                                        <span class="text-warning">invalid status</span>
                                        @endif
                                    </td>
                                    <td> {{$post->created_at->format('M d Y  @  H:i:s')}} </td>
                                    <td>
                                        <button type="button" class="btn btn-sm btn-outline-success" data-toggle="modal"
                                            data-target="#viewPost" data-title="{{$post->title}}"
                                            data-user="{{$post->user->name}}"
                                            data-date="{{$post->created_at->format('M d Y @ H:m:s')}}"
                                            data-body="{{$post->body}}" data-status="{{$post->status}}"
                                            data-image="{{ asset ('/images/posts/' . $post->image) }}">
                                            <i class="fa fa-eye"></i>
                                        </button>
                                        {{-- <button type="button" class="btn btn-sm btn-outline-primary" data-toggle="modal"
                                            data-target="#editPost" 
                                            data-title="{{$post->title}}"
                                        data-body="{{$post->body}}"
                                        data-status="{{$post->status}}"
                                        data-image="{{ asset ('/images/posts/' . $post->image) }}">
                                        <i class="fa fa-pencil"></i>
                                        </button> --}}
                                        <a href=" {{route('admin.editpost', $post->id)}} "
                                            class="btn btn-sm btn-outline-primary">
                                            <i class="fa fa-pencil"></i>
                                        </a>

                                        <button class="btn btn-sm btn-outline-danger" data-toggle="modal"
                                            data-target="#deletePost"
                                            data-name='Delete post "<strong>{{ $post->title }}</strong>"?'
                                            data-url="{{ route('admin.postdestroy', $post->id) }}">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <p>There is no post at the moment.</p>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <br>
                <strong>Active: <span class="text-primary">{{$active}}</span> </strong> &nbsp;&nbsp; <strong> Pending: <span class="text-danger">{{$pending}}</span> </strong>
            </div>
        </div>
    </div>
</div>

<!-- View Modal -->
<div class="modal fade" id="viewPost" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Post Preview</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <img class="card-img-top" id="image" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title" id="title"></h5>
                        <span>By: <small id="user"></small> </span>
                        <br>
                        <span>Created on: <small id="date"></small></span>
                        <br>
                        <hr>
                        <p id="body"></p>
                        <hr>
                        <strong>
                            <p class="card-text alert text-center" id="status"></p>
                        </strong>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Start of Delete Modal --}}
<div class="modal fade" id="deletePost" tabindex="-1" role="dialog" aria-labelledby="deleteModalTitle"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="color: black;">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalTitle">Delete?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid d-none alert  mb-3 text-center" id="deleteErrBox">
                    <h5><span id="deleteMsg"></span></h5>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
                <form action="" method="POST" id="delete_post_form">
                    <button type="submit" class="btn btn-outline-danger">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>
{{-- End of Delete Modal --}}

<script src="{{ asset('js/app.js') }}"></script>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script src="{{ asset('js/tinyMCE.js') }}"></script>

<script>
    $(document).ready(() => {

        $('.modal').on('hidden.bs.modal', function () {
            console.log('Modal closed.')
            $('#status').removeClass('alert-success')
            $('#status').removeClass('alert-danger')
            $(this).find('form').trigger('reset')
        });

        $('#viewPost').on('show.bs.modal', function (e) {
            var btn = $(e.relatedTarget)
            var modal = $(this)

            var title = modal.find('.modal-body #title').html(btn.data('title'))
            var body = modal.find('.modal-body #body').html(btn.data('body'))
            var image = modal.find('.modal-body #image').attr('src', btn.data('image'))
            var user = modal.find('.modal-body #user').html(btn.data('user'))
            var status = modal.find('.modal-body #status').html(btn.data('status'))
            var date = modal.find('.modal-body #date').html(btn.data('date'))
            var status1 = modal.find('.modal-body #status1').value
            var status2 = modal.find('.modal-body #status2').value


            if (btn.data('status') == 1) {
                $('#status').addClass('alert-success')
                status.html('ACTIVE')
            } else {
                $('#status').addClass('alert-danger')
                status.html('PENDING')
            }

        })



        $('#editPost').on('show.bs.modal', function (e) {
            var btn = $(e.relatedTarget)
            var modal = $(this)

            var title = modal.find('.modal-body #title').val(btn.data('title'))
            var body = modal.find('.modal-body #body').val(btn.data('body'))
            var image = modal.find('.modal-body #image').attr('src', btn.data('image'))

        })

        $('#deletePost').on('show.bs.modal', function (e) {
            let btn = $(e.relatedTarget)
            let modal = $(this)
            let name = modal.find('.modal-content #deleteModalTitle').html(btn.data('name'))
            let url = modal.find('.modal-content #delete_post_form').attr('action', btn.data('url'))

            document.getElementById('delete_post_form').addEventListener('submit', function (e) {
                e.preventDefault()
                axios.delete(this.action)
                    .then(response => {
                        if (response.data.error) {
                            // alert(response.data.error)
                            $('#deleteErrBox').removeClass('d-none')
                            $('#deleteErrBox').addClass('alert-success')
                            $('#deleteMsg').html(response.data.error)
                            // window.location.reload()
                        } else {
                            $('#deleteErrBox').removeClass('d-none')
                            $('#deleteErrBox').addClass('alert-success')
                            $('#deleteMsg').html('Post Deleted')
                            window.location.reload()
                        }
                       
                    })
                    .catch((error) => {
                        console.log(error.response)
                        let genErr = document.getElementById('deleteUserErr')
                        genErr.innerHTML = err.response.statusText
                        genErr.classList.remove('d-none')
                    })
            });
        });

        $('#select_all_posts').on('click', (e) => {
            $('.row_check_post').prop('checked', $(e.target).prop('checked'))
        })

        $('.row_check_post').on('click', () => {
            if ($('.row_check_post:checked').length == $('.row_check_post').length) {
                $('#select_all_posts').prop('checked', true)

            } else {
                $('#select_all_posts').prop('checked', false)
            }
        })


    });
</script>
@endsection

{{-- .then(response => {
    console.log(response.data.sms)
    $('#deleteErrBox').removeClass('d-none')
    $('#deleteErrBox').addClass('alert-success')
    $('#deleteMsg').html(response.data.sms)
    window.location.reload()
})
.catch((error) => {
    console.log(error.response)
    let genErr = document.getElementById('deleteUserErr')
    genErr.innerHTML = err.response.statusText
    genErr.classList.remove('d-none')
}) --}}