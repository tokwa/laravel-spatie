<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Post;
use App\Comment;

class CommentController extends Controller
{
    public function store(Request $request) {

        if (!Auth::check()) {

            return response()->json([
                'status' => 500,
                'sms' => ['You must login to make a comment.']
            ]);
        }

        if (! request()->ajax() && 
            !request()->isSecure()) {
            return response()->json(['error' => 'Failed to update.'], 500);
        }

        $request->validate([
            'comment' => ['required']
        ]);

        $comment = new Comment;
        $comment->user_id = auth()->id();
        $comment->post_id = $request->id;
        $comment->comment = $request->input('comment');
        

        if ($comment->save()) {
            
            return response()->json([
                'status' => 200,
                'sms' => 'Comment Posted.'
            ]);
        } else {

            return response()->json([
                'status' => 500,
                'sms' => 'Something went wrong =('
            ]);
        }

    }
}
