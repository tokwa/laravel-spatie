<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\DB;
use Auth;
use App\User;
use App\Post;
use App\Tag;
use App\Comment;

class AdminController extends Controller
{
    public function index() {

        $users = User::orderBy('created_at', 'desc')->paginate(10);
        $roles = Role::get();
        $permissions = Permission::get();

        return view('admin.index', compact('users', 'roles', 'permissions'));
    }
    
    public function store(Request $request) {

        $request->validate([
            'name' => ['required', 'string', 'max:255', 'unique:users'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'permissions' => ['nullable', 'exists:permissions,id'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        

        $user = new User;
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        
        if ($user->save()) {
            $user->syncPermissions(explode(',',$request->input(['permissions'])));
            return response()->json([
                'status' => 200,
                'sms' => 'User successfully created.'
            ]);
        }
        else {
            return response()->json([
                'status' => 500,
                'sms' => 'Something went wrong =('
            ]);
        }
    }

    public function update(Request $request, $id) {
        
        if (! request()->ajax() && 
            !request()->isSecure()) {
            return response()->json(['error' => 'Failed to update.'], 500);
        }

        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', 'unique:users,email,' . $id],
            'permissions' => ['nullable', 'exists:permissions,id'],
        ]);
        
        if (!empty ($request->password)) {
            $nopassword = "";
            $request->validate([
                'password' => ['string', 'min:8', 'confirmed']
            ]);
            $nopassword = $request->password;
        }

        $x = User::findOrFail($id);
        $x->name = $request->name;
        $x->email = $request->email;

        if (!empty($request->password)) {
            $x->password = Hash::make($nopassword);
        }

        if ($x->save()) {
            $x->syncPermissions($request->input('permissions'));

            return response()->json([
                'status' => 200,
                'sms' => 'User Successfully Edited!!',
            ]);
        }

        else {
            return response()->json([
                'status' => 500,
                'sms' => 'Something went wrong!!'
            ]);
        }
    }

    public function destroy($id) {

        if (! request()->ajax() && 
            !request()->isSecure()) {
            return response()->json(['error' => 'Something Went Wrong.'], 500);
        }

        $posts = Post::where('user_id', $id)->get();

        DB::beginTransaction();

        try {
            $posts = Post::where('user_id', $id)->get();
            
            foreach($posts as $post) {
                DB::table('tag_post')
                ->where('post_id', '=', $post->id)->delete();
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => $e->getMessage()]);
        }

        try {
            $comments = Comment::where('user_id', $id)->delete();
            $posts = Post::where('user_id', $id)->get();
            foreach ($posts as $post) {
                Comment::where([
                    ['post_id', '=', $post->id],
                ])->delete();
            }

        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => $e->getMessage()]);
        }

        try {
            Post::where('user_id', $id)->delete();
           
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => $e->getMessage()]);
        }

        try {
            User::findOrFail($id)->delete();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => $e->getMessage()]);
        }

        DB::commit();

    }

    public function deleteMultiple(Request $request) {

        if (! request()->ajax() && 
            !request()->isSecure()) {
            return response()->json(['error' => 'Something Went Wrong.'], 500);
        }

        $user = User::whereIn('id', $request->ids);

        DB::beginTransaction(); 

        try {
            $posts = Post::whereIn('user_id', $request->ids)->get();
            
            foreach($posts as $post) {
                DB::table('tag_post')
                ->where('post_id', '=', $post->id)->delete();
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => $e->getMessage()]);
        }
        
        try {
            $comment = Comment::whereIn('user_id', $request->ids)->delete();
            $posts = Post::whereIn('user_id', $request->ids)->get();

            foreach($posts as $post) {
                Comment::where([
                    ['post_id', '=', $post->id]
                ])->delete();
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => $e->getMessage()]);
        }

        try {
            $post = Post::whereIn('user_id', $request->ids)->delete();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => $e->getMessage()]);
        }

        try {
            $user->delete();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => $e->getMessage()]);
        }

        DB::commit();

    }

    //start of posts controller

    public function posts() {
        
        $posts = Post::orderBy('created_at', 'desc')->paginate(10);

        $active = Post::where('status', 1)->count();

        $pending = Post::where('status', 0)->count();

        return view('admin.posts', compact('posts', 'active', 'pending'));
    }

    public function editpost($post) {

        $tags = Tag::get();

        $post = Post::findOrFail($post);

        return view('admin.edit', compact('post','tags'));
    }

    public function updatepost(Request $request, $id)
    {
        if (! request()->ajax() && 
            !request()->isSecure()) {
            return response()->json(['error' => 'Failed to update.'], 500);
        }

        $post = Post::findOrFail($id);
        
        $request->validate([
            'title' => ['required'],
            'body' => ['required'],
            'tags' => ['required', 'exists:tags,id'],
            'status' => ['required']
        ]);

        if ($request->hasFile('image')) {
            $imgName = md5(date('Y-m-d') . "_" . microtime()) .
            "." . $request->file('image')->getClientOriginalExtension();
            $post->image = $imgName;
        }

        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->status = $request->input('status');

        if ($request->hasFile('image')) {
            $post->image = $imgName;
        }

        if ($post->save()) {

            if ($request->hasFile('image')) {
                $request->image->move(public_path('images/posts'), $imgName);
            }
            
            $post->tags()->detach();
            $post->tags()->attach(explode(',',$request->input('tags')));

            return response()->json([
                'status' => 200,
                'sms' => 'Successfully Edited!!',
                'x'
            ]);
        }
        else {
            return response()->json([
                'status' => 500,
                'sms' => 'Something went wrong!!'
            ]);
        }
    }

    public function destroypost($id) {

        if (!request()->ajax() && 
            !request()->isSecure()) {
            return response()->json(['error' => 'Something Went Wrong.'], 500);
        }

        $post = Post::findOrFail($id);

        DB::beginTransaction();  

        try {
            $comment = Comment::where('post_id', $id)->delete();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => $e->getMessage()]);
        }

        try {
            $post->delete();
            $post->tags()->detach();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => $e->getMessage()]);
        }

        DB::commit();

    }

}

//DESTROY POST

        // $post = Post::findOrFail($post);

        // if (!request()->ajax() && 
        //     !request()->isSecure()) {
        //     return response()->json(['error' => 'Something Went Wrong.'], 500);
        // }

        // if($post->delete()) {
        //     $post->tags()->detach();
        //     return response()->json([
        //         'status' => 200,
        //         'sms' => 'Post Successfully Deleted!!',
        //     ]);
        // }

        // else {
        //     return response()->json([
        //         'status' => 500,
        //         'sms' => 'Failed to Delete!!'
        //     ]);
        // }



// DELETE USER

        // if ($user->delete()) {

        //     if($user) {
        //         $permission = DB::table('model_has_permissions')
        //             ->whereIn('model_id', $request->ids)
        //                 ->delete();
        //     }

        //     return response()->json([
        //         'status' => 200,
        //         'sms' => 'Delete Successful!'
        //     ]);
        // }

        // else {
        //     return response()->json([
        //         'status' => 500,
        //         'sms' => 'Something Wentwrong!'
        //     ]);
        // }


        // $roles = DB::table('roles')
        //     ->join('model_has_roles', 'model_has_roles.role_id', '=', 'roles.id')
        //     ->join('users', 'users.id', '=', 'model_has_roles.model_id')
        //     ->where('users.id', $user)->get();

        // $allusers = DB::table('users')
        //     ->join('model_has_permissions', 'model_has_permissions.model_id', '=', 'users.id')
        //     ->where('users.id', 3)
        //     ->get();

        // $allpermissions = DB::table('permissions')->get();

        // $someusers = DB::table('users')->select('name', 'email', 'permission_id')
        // ->join('model_has_permissions', 'model_has_permissions.model_id', '=', 'users.id')
        // ->where('users.id', 1)->get();

        // dd($allusers);




        // DELETE USER

        // if (! request()->ajax() && 
//     !request()->isSecure()) {
//     return response()->json(['error' => 'Something Went Wrong.'], 500);
// }

// DB::beginTransaction();        

// try {
//     $post = Post::where('user_id', $id)->delete();
// } catch (\Exception $e) {
//     DB::rollBack();
//     return response()->json(['err' => $e->getMessage()]);
// }

// try {
//     $user = User::findOrFail($id)->delete();
// } catch (\Exception $e) {
//     DB::rollBack();
//     return response()->json(['err' => $e->getMessage()]);
// }

// DB::commit();


// MY AFFNEW ---------------- BREAK

// if ($req->hasFile('bigwin_file')) {
//     try {
//         Excel::import(new BigWinnersImport, $req->file('bigwin_file'), null, \Maatwebsite\Excel\Excel::CSV);
//         return redirect()->route('bigwinners.index')
//         ->withSuccess($this->success('Data', 'imported'));
//     } catch (\Exception $e) {
//         return redirect()->back()
//         ->withError($e->getMessage());
//     }
// }


// break--------------


        // if (! request()->ajax() && 
        //     !request()->isSecure()) {
        //     return response()->json(['error' => 'Something Went Wrong.'], 500);
        // }

        // $user = User::findOrFail($id);
        // $post = Post::where('user_id', $user->id)->delete();

        // if($post) {
        //     $user->delete();
        //     return response()->json([
        //         'status' => 200,
        //         'sms' => 'User Successfully Deleted!!',
        //     ]);
        // }

        // else {
        //     return response()->json([
        //         'status' => 500,
        //         'sms' => 'Failed to Delete!!'
        //     ]);
        // }