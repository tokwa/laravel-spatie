<?php

namespace App\Http\Controllers;

use App\Post;
use App\Tag;
use App\Comment;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::where('status', 1)
            ->orderBy('created_at', 'desc')
            ->paginate(4);

        return view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::allows('canCreatePost')) {
            $tags = Tag::get();
            return view('posts.create', compact('tags'));
        } else {
            abort (404);
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => ['required'],
            'body' => ['required'],
            'image' => ['required', 'image', 'max:2048'],
            'tags' => ['required', 'exists:tags,id']
        ]);

        $post = new Post;
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->user_id = auth()->id();

        if ($request->hasFile('image')) {
            $imgName = md5(date('Y-m-d') . "_" . microtime()) .
            "." . $request->file('image')->getClientOriginalExtension();
            $post->image = $imgName;
        }

        if ($post->save()) {
            $request->image->move(public_path('images/posts'), $imgName);
            $post->tags()->attach(explode(',',$request->input('tags')));
            return response()->json([
                'status' => 200,
                'sms' => 'Post Created.'
            ]);
        } else {
            return response()->json([
                'status' => '500',
                'sms' => 'Something went wrong =('
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $post = Post::where('slug', $slug)->firstOrFail();

        return view('posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        if (Gate::allows('canEditPost')) {

            $tags = Tag::get();
            $post = Post::where('slug', $slug)->firstOrFail();

            return view('posts.edit', compact('post', 'tags'));
        } else {
            abort (404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        if (! request()->ajax() && 
            !request()->isSecure()) {
            return response()->json(['error' => 'Failed to update.'], 500);
        }

        $request->validate([
            'title' => ['required'],
            'body' => ['required'],
            'tags' => ['required', 'exists:tags,id']
        ]);

        if ($request->hasFile('image')) {
            $imgName = md5(date('Y-m-d') . "_" . microtime()) .
            "." . $request->file('image')->getClientOriginalExtension();
            $post->image = $imgName;
        }

        $post->title = $request->input('title');
        $post->body = $request->input('body');

        if ($request->hasFile('image')) {
            $post->image = $imgName;
        }

        if ($post->save()) {

            if ($request->hasFile('image')) {
                $request->image->move(public_path('images/posts'), $imgName);
            }
            
            $post->tags()->detach();
            $post->tags()->attach(explode(',',$request->input('tags')));

            return response()->json([
                'status' => 200,
                'sms' => 'Successfully Edited!!',
                'x'
            ]);
        }
        else {
            return response()->json([
                'status' => 500,
                'sms' => 'Something went wrong!!'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!request()->ajax() && 
            !request()->isSecure()) {
            return response()->json(['error' => 'Something Went Wrong.'], 500);
        }

        $post = Post::findOrFail($id);

        DB::beginTransaction();  

        try {
            $comment = Comment::where('post_id', $id)->delete();
            $post->delete();
            $post->tags()->detach();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => $e->getMessage()]);
        }

        DB::commit();

    }
}



        // if($post->delete()) {
        //     $post->tags()->detach();
        //     return response()->json([
        //         'status' => 200,
        //         'sms' => 'Post Successfully Deleted!!',
        //     ]);
        // }

        // else {
        //     return response()->json([
        //         'status' => 500,
        //         'sms' => 'Failed to Delete!!'
        //     ]);
        // }