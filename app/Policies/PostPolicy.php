<?php

namespace App\Policies;

use App\User;
use Auth;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function canCreatePost($user) {
        if (Auth::check() && auth()->user()->hasPermissionTo('create post')) {
            return $user = true;
        }
    }

    public function canEditPost($user) {
        if (Auth::check() && auth()->user()->hasPermissionTo('edit post')) {
            return $user = true;
        }
    }
}
