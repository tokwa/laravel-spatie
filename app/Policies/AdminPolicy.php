<?php

namespace App\Policies;

use App\User;
use Auth;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdminPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    public function canManageUsers($user) {        
        if (Auth::check() && auth()->user()->hasPermissionTo('manage users')) {
            return $user = true;
        } else {
            return $user = false;
        }
    }
}
